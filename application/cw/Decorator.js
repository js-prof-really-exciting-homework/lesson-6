import { SmsNotifier, SlackNotifier,  ViberNotifier, GmailNotifier, TelegramNotifier, SkypeNotifier, MessengerNotifier } from './notifier';

class BaseDecorator {
  constructor( clients ){
    let obs = clients.map( obs => {
      if( obs.name === 'sms' ){
        return new SmsNotifier(obs);
      } else if( obs.name === 'gmail'){
        return new GmailNotifier(obs);
      } else if( obs.name === 'telegram'){
        return new TelegramNotifier(obs);
      } else if( obs.name === 'viber'){
        return new ViberNotifier(obs);
      } else if( obs.name === 'slack'){
        return new SlackNotifier(obs);
      } else if( obs.name === 'skype' ){
        return new SkypeNotifier(obs);
      } else if( obs.name === 'messenger' ){
        return new MessengerNotifier(obs);
      }

    });
    this.clients = obs;
  }
  sendMessage( msg, node ){
    this.clients.map( ( obs ) => {
      obs.send( msg, node );
    });
  }
}


export default BaseDecorator;