import Human from './human';

export function PartyMan ( Human ){
        this.name = Human.name,
        this.coolers = [],
        this.currentTemperature = 0;
        this.minTemperature = -10;
        this.maxTemperature = 30;

        this.addCooler = function(name=undefined,temperatureCoolRate=undefined){
          let cooler = {};
          if (name !== undefined && name !== ''){
            cooler.name = name
          } else {
            return console.error('specify name');
          }

          if (temperatureCoolRate !== undefined){
            cooler.temperatureCoolRate = temperatureCoolRate
          } else {
            console.error('specify cool rate');
          }

          this.coolers.push(cooler);
        }
      }

      PartyMan.prototype = Human.prototype;

export default PartyMan;