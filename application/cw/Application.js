import Notifier from './Decorator';
import NotifTarget from './NotifTarget';

class Application {

	constructor(){

		this.notifierTargets = [];		
		this.renderInterface = this.renderInterface.bind(this);
		this.node = null;
		if (arguments.length !== 0){			
			let args = Array.from(arguments);
				args.map(arg => {
					let notifElem = new NotifTarget(arg);
					this.notifierTargets.push(notifElem);
				});
		}
		this.notifier = new Notifier(this.notifierTargets);		
	}

	renderInterface(){
		const root = document.getElementById('root');

		const appWrap = document.createElement('section');
		appWrap.classList.add('notifier_app');

		appWrap.innerHTML =
		`
		<div class="notifier_app--container">
			<header>
				<input type="text" class="notifier__messanger" />
				<button class="notifier__send">Send Message</button>
			</header>
	        <div class="notifier__container">
	        ${
	          this.notifierTargets.map( item =>
	            `
	            <div class="notifier__item" data-name="${item.name}">
	              <header class="notifier__header">
	                <img width="25" src="${item.image}"/>
	                <span>${item.name}</span>
	              </header>
	              <div class="notifier__messages"></div>
	            </div>
	            `).join('')
	        }
	        </div>			
		</div>
		`;
    	const btn = appWrap.querySelector('.notifier__send');
    	const input = appWrap.querySelector('.notifier__messanger');
    		this.node = appWrap;
	    	btn.addEventListener('click', () => {
	    		let value = input.value;
	    		this.notifier.sendMessage(value, this.node);
	    	});
		    
	    root.appendChild(appWrap);

	}
}

export default Application;