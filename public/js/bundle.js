/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/DecoratorExample/Notifier.js":
/*!**************************************************!*\
  !*** ./application/DecoratorExample/Notifier.js ***!
  \**************************************************/
/*! exports provided: SmsNotifier, ViberNotifier, GmailNotifier, TelegramNotifier, SlackNotifier, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SmsNotifier\", function() { return SmsNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ViberNotifier\", function() { return ViberNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"GmailNotifier\", function() { return GmailNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TelegramNotifier\", function() { return TelegramNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SlackNotifier\", function() { return SlackNotifier; });\nclass Notifier {\n  send( msg, baseNode, block ){\n      console.log('CLASS NOTIFIER: mesasge was sended:', msg );\n      const target = baseNode.querySelector(`.notifier__item[data-slug=\"${block}\"]`);\n      console.log('target', target);\n      target.innerHTML += `<div>${msg}</div>`;\n  }\n}\n\n\n/*\n\n  // Пока не смотрит вниз :)\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n*/\n\nclass SmsNotifier extends Notifier {\n  send( msg, baseNode, block = 'sms' ){\n\n    ///....\n    // fetch('kyivstar.ua/send?...')\n    super.send(msg, baseNode, block);\n  }\n}\n\nclass ViberNotifier extends Notifier {\n  send( msg, baseNode, block = 'viber'){\n    //...\n    // fetch('viber.com/send?...')\n      super.send(msg, baseNode, block);\n  }\n}\n\nclass GmailNotifier extends Notifier {\n  send( msg, baseNode, block = 'mail' ){\n      // fetch('gmail.com/send?...')\n      super.send(msg, baseNode, block);\n  }\n}\n\nclass TelegramNotifier extends Notifier {\n  send( msg, baseNode, block = 'telegram' ){\n      // fetch('telegram.com/send?...')\n      super.send(msg, baseNode, block);\n  }\n}\n\nclass SlackNotifier extends Notifier {\n  send( msg, baseNode, block = 'slack' ){\n      // fetch('slack.com/send?...')\n      super.send(msg, baseNode, block);\n  }\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Notifier);\n\n\n//# sourceURL=webpack:///./application/DecoratorExample/Notifier.js?");

/***/ }),

/***/ "./application/DecoratorExample/baseDecorator.js":
/*!*******************************************************!*\
  !*** ./application/DecoratorExample/baseDecorator.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Notifier__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Notifier */ \"./application/DecoratorExample/Notifier.js\");\n\n\nclass BaseDecorator {\n  constructor( clients ){\n    let obs = clients.map( obs => {\n      if( obs.name === 'sms' ){\n        return new _Notifier__WEBPACK_IMPORTED_MODULE_0__[\"SmsNotifier\"](obs);\n      } else if( obs.name === 'mail'){\n        return new _Notifier__WEBPACK_IMPORTED_MODULE_0__[\"GmailNotifier\"](obs);\n      } else if( obs.name === 'telegram'){\n        return new _Notifier__WEBPACK_IMPORTED_MODULE_0__[\"TelegramNotifier\"](obs);\n      } else if( obs.name === 'viber'){\n        return new _Notifier__WEBPACK_IMPORTED_MODULE_0__[\"ViberNotifier\"](obs);\n      } else if( obs.name === 'slack'){\n        return new _Notifier__WEBPACK_IMPORTED_MODULE_0__[\"SlackNotifier\"](obs);\n      }\n    })\n    this.clients = obs;\n  }\n  sendMessage( msg, baseNode ){\n    this.clients.map( ( obs ) => {\n      obs.send(msg, baseNode);\n    });\n  }\n\n  addNotifier( notifier ){\n    this.clients.push( notifier );\n  }\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BaseDecorator);\n\n\n//# sourceURL=webpack:///./application/DecoratorExample/baseDecorator.js?");

/***/ }),

/***/ "./application/DecoratorExample/index.js":
/*!***********************************************!*\
  !*** ./application/DecoratorExample/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _baseDecorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./baseDecorator */ \"./application/DecoratorExample/baseDecorator.js\");\n\n// import Notifier from './Notifier';\n\n\nclass Application{\n  constructor(){\n    this.notifierTargets = [\n      {name: 'sms', image: 'images/sms.svg'},\n      {name: 'mail', image: 'images/gmail.svg'},\n      {name: 'telegram', image: 'images/telegram.svg'},\n      {name: 'viber', image: 'images/viber.svg'},\n      {name: 'slack', image: 'images/slack.svg'},\n    ];\n    this.notifier = new _baseDecorator__WEBPACK_IMPORTED_MODULE_0__[\"default\"]( this.notifierTargets );\n    this.createInterface = this.createInterface.bind(this);\n    this.node = null;\n  }\n\n  createInterface(){\n    const root = document.getElementById('root');\n    const AppNode = document.createElement('section');\n\n    AppNode.className = 'notifer_app';\n      console.log( this.notifierTargets);\n    AppNode.innerHTML =\n    `\n      <div class=\"notifer_app--container\">\n        <header>\n          <input class=\"notifier__messanger\" type=\"text\"/>\n          <button class=\"notifier__send\">Send Message</button>\n        </header>\n        <div class=\"notifier__container\">\n        ${\n          this.notifierTargets.map( item =>\n            `\n            <div class=\"notifier__item\" data-slug=\"${item.name}\">\n              <header class=\"notifier__header\">\n                <img width=\"25\" src=\"${item.image}\"/>\n                <span>${item.name}</span>\n              </header>\n              <div class=\"notifier__messages\"></div>\n            </div>\n            `).join('')\n        }\n        </div>\n      </div>\n    `;\n\n    const btn = AppNode.querySelector('.notifier__send');\n    const input = AppNode.querySelector('.notifier__messanger');\n    btn.addEventListener('click', () => {\n      let value = input.value;\n\n      this.notifier.sendMessage(value, 'sms');\n\n      // this.notifier.send(value, 'sms');\n      // this.notifier.send(value, 'viber');\n      // this.notifier.send(value, 'telegram');\n      // this.notifier.send(value, 'viber');\n\n    });\n\n    // this.node = AppNode;\n    root.appendChild(AppNode);\n  }\n\n}\n\n\nconst Demo = () => {\n  const NotifierApp = new Application();\n  console.log(NotifierApp);\n  NotifierApp.createInterface();\n\n  const NotApp = new Application();\n  NotApp.createInterface();\n\n  const NotApp2 = new Application();\n  NotApp2.createInterface();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo);\n\n\n//# sourceURL=webpack:///./application/DecoratorExample/index.js?");

/***/ }),

/***/ "./application/cw/Application.js":
/*!***************************************!*\
  !*** ./application/cw/Application.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Decorator */ \"./application/cw/Decorator.js\");\n/* harmony import */ var _NotifTarget__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NotifTarget */ \"./application/cw/NotifTarget.js\");\n\r\n\r\n\r\nclass Application {\r\n\r\n\tconstructor(){\r\n\r\n\t\tthis.notifierTargets = [];\t\t\r\n\t\tthis.renderInterface = this.renderInterface.bind(this);\r\n\t\tthis.node = null;\r\n\t\tif (arguments.length !== 0){\t\t\t\r\n\t\t\tlet args = Array.from(arguments);\r\n\t\t\t\targs.map(arg => {\r\n\t\t\t\t\tlet notifElem = new _NotifTarget__WEBPACK_IMPORTED_MODULE_1__[\"default\"](arg);\r\n\t\t\t\t\tthis.notifierTargets.push(notifElem);\r\n\t\t\t\t});\r\n\t\t}\r\n\t\tthis.notifier = new _Decorator__WEBPACK_IMPORTED_MODULE_0__[\"default\"](this.notifierTargets);\t\t\r\n\t}\r\n\r\n\trenderInterface(){\r\n\t\tconst root = document.getElementById('root');\r\n\r\n\t\tconst appWrap = document.createElement('section');\r\n\t\tappWrap.classList.add('notifier_app');\r\n\r\n\t\tappWrap.innerHTML =\r\n\t\t`\r\n\t\t<div class=\"notifier_app--container\">\r\n\t\t\t<header>\r\n\t\t\t\t<input type=\"text\" class=\"notifier__messanger\" />\r\n\t\t\t\t<button class=\"notifier__send\">Send Message</button>\r\n\t\t\t</header>\r\n\t        <div class=\"notifier__container\">\r\n\t        ${\r\n\t          this.notifierTargets.map( item =>\r\n\t            `\r\n\t            <div class=\"notifier__item\" data-name=\"${item.name}\">\r\n\t              <header class=\"notifier__header\">\r\n\t                <img width=\"25\" src=\"${item.image}\"/>\r\n\t                <span>${item.name}</span>\r\n\t              </header>\r\n\t              <div class=\"notifier__messages\"></div>\r\n\t            </div>\r\n\t            `).join('')\r\n\t        }\r\n\t        </div>\t\t\t\r\n\t\t</div>\r\n\t\t`;\r\n    \tconst btn = appWrap.querySelector('.notifier__send');\r\n    \tconst input = appWrap.querySelector('.notifier__messanger');\r\n    \t\tthis.node = appWrap;\r\n\t    \tbtn.addEventListener('click', () => {\r\n\t    \t\tlet value = input.value;\r\n\t    \t\tthis.notifier.sendMessage(value, this.node);\r\n\t    \t});\r\n\t\t    \r\n\t    root.appendChild(appWrap);\r\n\r\n\t}\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Application);\n\n//# sourceURL=webpack:///./application/cw/Application.js?");

/***/ }),

/***/ "./application/cw/Decorator.js":
/*!*************************************!*\
  !*** ./application/cw/Decorator.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _notifier__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notifier */ \"./application/cw/notifier.js\");\n\r\n\r\nclass BaseDecorator {\r\n  constructor( clients ){\r\n    let obs = clients.map( obs => {\r\n      if( obs.name === 'sms' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"SmsNotifier\"](obs);\r\n      } else if( obs.name === 'gmail'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"GmailNotifier\"](obs);\r\n      } else if( obs.name === 'telegram'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"TelegramNotifier\"](obs);\r\n      } else if( obs.name === 'viber'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"ViberNotifier\"](obs);\r\n      } else if( obs.name === 'slack'){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"SlackNotifier\"](obs);\r\n      } else if( obs.name === 'skype' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"SkypeNotifier\"](obs);\r\n      } else if( obs.name === 'messenger' ){\r\n        return new _notifier__WEBPACK_IMPORTED_MODULE_0__[\"MessengerNotifier\"](obs);\r\n      }\r\n\r\n    });\r\n    this.clients = obs;\r\n  }\r\n  sendMessage( msg, node ){\r\n    this.clients.map( ( obs ) => {\r\n      obs.send( msg, node );\r\n    });\r\n  }\r\n}\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (BaseDecorator);\n\n//# sourceURL=webpack:///./application/cw/Decorator.js?");

/***/ }),

/***/ "./application/cw/NotifTarget.js":
/*!***************************************!*\
  !*** ./application/cw/NotifTarget.js ***!
  \***************************************/
/*! exports provided: NotifTarget, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"NotifTarget\", function() { return NotifTarget; });\nclass NotifTarget {\r\n\tconstructor(name){\r\n\t\tthis.name = name;\r\n\t\tthis.image = `images/${this.name}.svg`\r\n\t}\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (NotifTarget);\n\n//# sourceURL=webpack:///./application/cw/NotifTarget.js?");

/***/ }),

/***/ "./application/cw/PartyMan.js":
/*!************************************!*\
  !*** ./application/cw/PartyMan.js ***!
  \************************************/
/*! exports provided: PartyMan, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"PartyMan\", function() { return PartyMan; });\n/* harmony import */ var _human__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./human */ \"./application/cw/human.js\");\n\r\n\r\nfunction PartyMan ( Human ){\r\n        this.name = Human.name,\r\n        this.coolers = [],\r\n        this.currentTemperature = 0;\r\n        this.minTemperature = -10;\r\n        this.maxTemperature = 30;\r\n\r\n        this.addCooler = function(name=undefined,temperatureCoolRate=undefined){\r\n          let cooler = {};\r\n          if (name !== undefined && name !== ''){\r\n            cooler.name = name\r\n          } else {\r\n            return console.error('specify name');\r\n          }\r\n\r\n          if (temperatureCoolRate !== undefined){\r\n            cooler.temperatureCoolRate = temperatureCoolRate\r\n          } else {\r\n            console.error('specify cool rate');\r\n          }\r\n\r\n          this.coolers.push(cooler);\r\n        }\r\n      }\r\n\r\n      PartyMan.prototype = _human__WEBPACK_IMPORTED_MODULE_0__[\"default\"].prototype;\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (PartyMan);\n\n//# sourceURL=webpack:///./application/cw/PartyMan.js?");

/***/ }),

/***/ "./application/cw/human.js":
/*!*********************************!*\
  !*** ./application/cw/human.js ***!
  \*********************************/
/*! exports provided: Human, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Human\", function() { return Human; });\nfunction Human( name ){\r\n          this.name = name;\r\n          this.currentTemperature = 0;\r\n          this.minTemperature = -10;\r\n          this.maxTemperature = 50;\r\n\r\n          console.log( this, `new Human ${this.name} arrived!`);\r\n        }\r\n\r\n        Human.prototype.ChangeTemperature = function( changeValue ){\r\n          console.log(`current temperature has changed on ${changeValue} degrees`);\r\n          this.currentTemperature = this.currentTemperature + changeValue;\r\n\r\n\r\n          if ( this.currentTemperature >= 0 && this.currentTemperature <= this.maxTemperature){            \r\n            console.log(`It's getting hot make sure to have enogh coolers (current temp: ${this.currentTemperature}, max. temp: ${this.maxTemperature})`)\r\n          } else if ( this.currentTemperature > this.maxTemperature ) {\r\n            if (this.coolers.length !== 0){                                          \r\n              while ( this.currentTemperature > this.maxTemperature && this.coolers.length !== 0) {\r\n                this.currentTemperature -= Number(this.coolers[0].temperatureCoolRate);\r\n                console.log(`You used one of your coolers. It was ${this.coolers[0].name}. Current temp is: ${this.currentTemperature}, ${this.coolers.length-1} coolers remaining`);\r\n                this.coolers.splice(0,1);\r\n              };\r\n              \r\n               if (this.currentTemperature > this.maxTemperature) {\r\n                if (this.coolers.length === 0) {\r\n                  console.error(`temp is too hot (${this.currentTemperature}) and you didn't have enough coolers so you have burnt alive :(`)\r\n                } \r\n              }\r\n            } else if (this.coolers.length === 0) {\r\n              console.error(`temp is too hot (${this.currentTemperature}) and you didn't have enough coolers so you have burnt alive :(`)\r\n            }\r\n          } else if( this.currentTemperature < this.minTemperature ){\r\n            console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);\r\n          } else if ( this.currentTemperature < 0 ) {\r\n            console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);\r\n          }\r\n        };\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Human);\n\n//# sourceURL=webpack:///./application/cw/human.js?");

/***/ }),

/***/ "./application/cw/notifier.js":
/*!************************************!*\
  !*** ./application/cw/notifier.js ***!
  \************************************/
/*! exports provided: Notifier, SmsNotifier, ViberNotifier, GmailNotifier, TelegramNotifier, SlackNotifier, SkypeNotifier, MessengerNotifier, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Notifier\", function() { return Notifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SmsNotifier\", function() { return SmsNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ViberNotifier\", function() { return ViberNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"GmailNotifier\", function() { return GmailNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TelegramNotifier\", function() { return TelegramNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SlackNotifier\", function() { return SlackNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SkypeNotifier\", function() { return SkypeNotifier; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"MessengerNotifier\", function() { return MessengerNotifier; });\nclass Notifier {\r\n\tsend (msg, node, name){\r\n\t\tconsole.log(`message has been sent: ${msg} via ${name}`);\r\n\t\tconst target = node.querySelector(`.notifier__item[data-name=\"${name}\"]`);\r\n\t\ttarget.innerHTML += `<div>${msg}</div>`;\r\n\t}\r\n}\r\n\r\nclass SmsNotifier extends Notifier {\r\n  send( msg, node, name = 'sms' ){\r\n    super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass ViberNotifier extends Notifier {\r\n  send( msg, node, name = 'viber'){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass GmailNotifier extends Notifier {\r\n  send( msg, node, name = 'gmail' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass TelegramNotifier extends Notifier {\r\n  send( msg, node, name = 'telegram' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass SlackNotifier extends Notifier {\r\n  send( msg, node, name = 'slack' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass SkypeNotifier extends Notifier {\r\n  send( msg, node, name = 'skype' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\nclass MessengerNotifier extends Notifier {\r\n  send( msg, node, name = 'messenger' ){\r\n      super.send(msg, node, name);\r\n  }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Notifier);\n\n//# sourceURL=webpack:///./application/cw/notifier.js?");

/***/ }),

/***/ "./application/decorator/basicUsage.js":
/*!*********************************************!*\
  !*** ./application/decorator/basicUsage.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst BaseDemo = () => {\n\n    console.log( 'DECORATOR BASE!');\n\n      function Human( name ){\n        this.name = name;\n        this.currentTemperature = 0;\n        this.minTemperature = -10;\n\n        console.log( this, `new Human ${this.name} arrived!`);\n      }\n\n      Human.prototype.ChangeTemperature = function( changeValue ){\n        console.log(\n          'current', this.currentTemperature + changeValue,\n          'min', this.minTemperature\n        );\n\n        this.currentTemperature = this.currentTemperature + changeValue;\n        if( this.currentTemperature < this.minTemperature ){\n          console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);\n        } else {\n          console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);\n        }\n      };\n\n      let Morgan = new Human('Morgan');\n          Morgan.ChangeTemperature(-5);\n          Morgan.ChangeTemperature(-6);\n\n      function DressedHuman( Human ){\n        this.name = Human.name;\n        this.clothes = [\n          { name: 'jacket', temperatureResistance: 20},\n          { name: 'hat', temperatureResistance: 5},\n          { name: 'scarf', temperatureResistance: 10},\n        ];\n        this.currentTemperature = 0;\n        this.minTemperature = Human.minTemperature - this.clothes.reduce(\n            (currentResistance, clothe ) => {\n              console.log('currentResistance', currentResistance,  'clothe', clothe);\n              return currentResistance + clothe.temperatureResistance;\n            }, 0\n          );\n        console.log(`new Human ${this.name} arrived! He can survive in temperature ${this.minTemperature}`, this);\n      }\n      DressedHuman.prototype = Human.prototype;\n\n      let Dexter = new DressedHuman( new Human('Dexter') );\n      // console.log( Dexter );\n          Dexter.ChangeTemperature(-6);\n          Dexter.ChangeTemperature(-16);\n          Dexter.ChangeTemperature(-16);\n          Dexter.ChangeTemperature(-26);\n\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BaseDemo);\n\n\n//# sourceURL=webpack:///./application/decorator/basicUsage.js?");

/***/ }),

/***/ "./application/decorator/basicUsage_es6.js":
/*!*************************************************!*\
  !*** ./application/decorator/basicUsage_es6.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst DecoratorClass = () => {\n\n  console.log( 'DECORATOR ON CLASSES!');\n\n  class Human {\n    constructor(name){\n      this.name = name;\n      this.currentTemperature = 0;\n      this.minTemperature = -10;\n\n      console.log(`new Human ${this.name} arrived!`, this);\n    }\n\n    changeTemperature(changeValue){\n      console.log(\n        'current', this.currentTemperature + changeValue,\n        'min', this.minTemperature\n      );\n      let prevTemperature = this.currentTemperature;\n      this.currentTemperature = this.currentTemperature + changeValue;\n\n      if( this.currentTemperature < this.minTemperature ){\n        console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);\n      } else {\n        if( this.currentTemperature > prevTemperature  ) {\n          console.log(`Temperature is growing. Seems someone go to Odessa or drink some hot tea?`)\n        } else {\n          console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);\n        }\n      }\n    }\n  }\n\n  let Debra = new Human('Debra');\n      Debra.changeTemperature(-5);\n      Debra.changeTemperature(6);\n      Debra.changeTemperature(-16);\n\n  class DressedHuman extends Human{\n    constructor(name){\n      super(name);\n      this.clothes = [\n        { name: 'jacket', temperatureResistance: 20},\n        { name: 'hat', temperatureResistance: 5},\n        { name: 'scarf', temperatureResistance: 10},\n      ];\n      this.minTemperature = this.minTemperature - this.clothes.reduce(\n          (currentResistance, clothe ) => {\n            console.log('currentResistance', currentResistance,  'clothe', clothe);\n            return currentResistance + clothe.temperatureResistance;\n          }, 0\n        );\n      console.log(`Dressed Human ${name}`, this);\n    }\n  }\n\n  let Masuka = new DressedHuman('Masuka');\n      Masuka.changeTemperature(-25);\n      Masuka.changeTemperature(-26);\n\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (DecoratorClass);\n\n\n//# sourceURL=webpack:///./application/decorator/basicUsage_es6.js?");

/***/ }),

/***/ "./application/decorator/index.js":
/*!****************************************!*\
  !*** ./application/decorator/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _basicUsage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basicUsage */ \"./application/decorator/basicUsage.js\");\n/* harmony import */ var _basicUsage_es6__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basicUsage_es6 */ \"./application/decorator/basicUsage_es6.js\");\n\n\n\n\n/*\n  Декоратор — это структурный паттерн проектирования,\n  который позволяет динамически добавлять объектам новую\n  функциональность, оборачивая их в полезные «обёртки».\n\n  https://refactoring.guru/ru/design-patterns/decorator\n*/\n\nconst DecoratorDemo = () => {\n\n  // console.log( 'DECORATOR AS DESIGN PATTERN DEMO!');\n  // Base();\n  // BaseClass();\n  // console.log( '- - - - - - - - - - - -');\n  // Define();  \n\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (DecoratorDemo);\n\n\n//# sourceURL=webpack:///./application/decorator/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _DecoratorExample__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DecoratorExample */ \"./application/DecoratorExample/index.js\");\n/* harmony import */ var _decorator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./decorator */ \"./application/decorator/index.js\");\n/* harmony import */ var _classworks_task1__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classworks/task1 */ \"./classworks/task1.js\");\n/* harmony import */ var _classworks_task2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classworks/task2 */ \"./classworks/task2.js\");\n\n\n\n\n\n/*\n  Демо декоратора\n*/\n// DecoratorBase();\n// DecoratorExample();\n// BeachParty();\nObject(_classworks_task2__WEBPACK_IMPORTED_MODULE_3__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/task1.js":
/*!*****************************!*\
  !*** ./classworks/task1.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_cw_human__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/cw/human */ \"./application/cw/human.js\");\n/* harmony import */ var _application_cw_PartyMan__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../application/cw/PartyMan */ \"./application/cw/PartyMan.js\");\n/*\n\n  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.\n\n  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,\n      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты\n      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}\n\n  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,\n      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers\n      и \"охлаждаем\" человека на ту температуру которая там указана.\n\n      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.\n      Если температура превышает 50 градусов, выводить сообщение error -> \"{Human.name} зажарился на солнце :(\"\n\n  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который\n      будет добавлять \"охладитель\" в наш обьект. Сделать валидацию что бы через этот метод\n      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.\n      Выводить сообщение с ошибкой про это.\n\n*/\n\n\n\n\n//Использовал декоратор не на классах т.к. в условии написано \"из примера basicUsage\", а не из примера basicUsage_es6 ;)\n\nconst BeachParty = () => {\n\n  let Bilbo =  new _application_cw_PartyMan__WEBPACK_IMPORTED_MODULE_1__[\"default\"]( new _application_cw_human__WEBPACK_IMPORTED_MODULE_0__[\"default\"]( 'Bilbo' ) );\n  Bilbo.addCooler('icecream', 20);\n  Bilbo.addCooler('juice', 10);\n  Bilbo.addCooler('beer', 35);\n  Bilbo.addCooler('',35);\n  Bilbo.ChangeTemperature(30);\n  Bilbo.ChangeTemperature(10);\n  Bilbo.ChangeTemperature(40);\n  Bilbo.ChangeTemperature(60);\n \n  console.log(Bilbo);\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (BeachParty);\n\n\n//# sourceURL=webpack:///./classworks/task1.js?");

/***/ }),

/***/ "./classworks/task2.js":
/*!*****************************!*\
  !*** ./classworks/task2.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/cw/Application */ \"./application/cw/Application.js\");\n/* harmony import */ var _application_cw_notifier__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../application/cw/notifier */ \"./application/cw/notifier.js\");\n\n/*\n  Повторить задание с оповещаниями (application/DecoratorExample), с\n  использованием нескольких уровней абстракций, а именно паттерны:\n  Decorator, Observer, Fabric\n\n\n  Задача: Написать динамичную систему оповещений, которая будет отправлять\n  сообщения все подписаным на неё \"Мессенджерам\".\n  Картинки мессенджеров есть в папке public/images\n\n  Класс оповещения должен иметь декоратор на каждый мессенджер.\n\n  При создании обьекта класса Application нужно передавать обьект\n  в котором будут находится те \"Мессенджеры\" который в результате будут\n  подписаны на этот блок приложения.\n\n  Отправка сообщения по \"мессенджерам\" должна происходить при помощи\n  паттерна Observer.\n\n  При отправке сообщения нужно создавать обьект соответствующего класса,\n  для каждого типа оповещания.\n\n  let header = new Application('slack', 'viber', 'telegram');\n  let feedback = new Application('skype', 'messenger', 'mail', 'telegram');\n\n  btn.addEventListener('click', () => header.sendMessage(msg) );\n\n  Архитектура:\n\n  Application( messangers ) ->\n    notifier = new Notifier\n    renderInterface(){...}\n\n  Notifier ->\n    constructor() ->\n      Fabric-> Фабрикой перебираете все типы месенджеров которые\n      подписаны на эту Application;\n    send() -> Отправляет сообщение всем подписчикам\n*/\n\n\n\n\nlet NotificationApp = () => {\n  let header = new _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('slack', 'viber', 'telegram', 'skype', 'messenger'); //Сделал конструктор объекта из аргументов, имхо так удобнее (см. NotifTarget.js)\n  let feedback = new _application_cw_Application__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('skype', 'messenger', 'gmail', 'telegram');\n  header.renderInterface();\n  feedback.renderInterface();\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (NotificationApp);\n\n//# sourceURL=webpack:///./classworks/task2.js?");

/***/ })

/******/ });